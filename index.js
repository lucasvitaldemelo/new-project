const express = require('express')
const bodyParser = require('body-parser');
const app = express();

app.use(express.json())
app.use(bodyParser.urlencoded({ extended: true }))
const port = 8000


//• Get :
// 1º.) parâmetro de envio via query (atributo nome)

app.get('/clientes', (req, res) => {
    const name = req.query
    res.send(name.nome)
})

// • Post:
// 1º) criar um parâmetro no header chamado access;
// 2º) parâmetros de envio via body;
// 3º.) validar se o valor que foi enviado no header está correto para prosseguir com a execução;

app.post('/clientes', (req, res) => {
    const body = req.body
    const header = req.headers

    if (header.access) {
        res.json({access: header.access, body})
    } else {
        console.log("Inválido")
    }
})

// • Delete:
// 1º) criar um parâmetro no header chamado access;
// 2º) parâmetros de envio via body;
// 3º.) validar se o valor que foi enviado no header está correto para prosseguir com a execução;

app.delete('/clientes/:acesso', (req, res) => {
    const param = req.params.acesso
    const header = req.headers

    if (header.access) {
        res.json({access: header.access, param})
    } else {
        console.log("Inválido")
    }
})

// • Put:
// 1º) criar um parâmetro no header chamado access;
// 2º) parâmetros de envio via body;
// 3º.) validar se o valor que foi enviado no header está correto para prosseguir com a execução;

app.put('/clientes', (req, res) => {
    const body = req.body
    const header = req.headers

    if (header.access) {
        res.json({access: header.access, body})
    } else {
        console.log("Inválido")
    }
})


app.get('/funcionario', (req, res) => {
    const name = req.query
    res.send(name.nome)
})

// • Post:
// 1º) criar um parâmetro no header chamado access;
// 2º) parâmetros de envio via body;
// 3º.) validar se o valor que foi enviado no header está correto para prosseguir com a execução;

app.post('/funcionario', (req, res) => {
    const body = req.body
    const header = req.headers

    if (header.access) {
        res.json({access: header.access, body})
    } else {
        console.log("Inválido")
    }
})

// • Delete:
// 1º) criar um parâmetro no header chamado access;
// 2º) parâmetros de envio via body;
// 3º.) validar se o valor que foi enviado no header está correto para prosseguir com a execução;

app.delete('/funcionario/:acesso', (req, res) => {
    const param = req.params.acesso
    const header = req.headers

    if (header.access) {
        res.json({access: header.access, param})
    } else {
        console.log("Inválido")
    }
})

// • Put:
// 1º) criar um parâmetro no header chamado access;
// 2º) parâmetros de envio via body;
// 3º.) validar se o valor que foi enviado no header está correto para prosseguir com a execução;

app.put('/funcionario', (req, res) => {
    const body = req.body
    const header = req.headers

    if (header.access) {
        res.json({access: header.access, body})
    } else {
        console.log("Inválido")
    }
})



app.listen(port, () => {
    console.log(`--------------------------------`)
    console.log(`Projeto iniciado na porta: ${port}`)
    console.log(`--------------------------------`)
})